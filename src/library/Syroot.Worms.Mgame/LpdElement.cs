﻿using System;
using System.Drawing;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents a single UI element described in an <see cref="Lpd"/> file.
    /// </summary>
    public class LpdElement
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a the relative path to the IGD storing the images of this element.
        /// </summary>
        public string FileName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets a the area at which the UI element appears.
        /// </summary>
        public Rectangle Rectangle { get; set; }

        public int Properties { get; set; }

        /// <summary>
        /// Gets or sets a value which must lie between 0-4 (inclusive) for the game to accept the element.
        /// </summary>
        public int Version { get; set; }
    }
}
