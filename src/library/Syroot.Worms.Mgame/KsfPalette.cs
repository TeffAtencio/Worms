﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Syroot.BinaryData;
using Syroot.Worms.Graphics;
using Syroot.Worms.IO;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents a color palette referenced by <see cref="KsfImage"/> data.
    /// </summary>
    public class KsfPalette : ILoadableFile, IPalette
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>Number of colors stored in a palette.</summary>
        internal const int ColorCount = 256;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="KsfPalette"/> class.
        /// </summary>
        public KsfPalette() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="KsfPalette"/> class, loading data from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public KsfPalette(string fileName) => Load(fileName);

        /// <summary>
        /// Initializes a new instance of the <see cref="KsfPalette"/> class, loading data from the given
        /// <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public KsfPalette(Stream stream) => Load(stream);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the array of 256 colors stored in this palette.
        /// </summary>
        public IList<Color> Colors { get; set; } = new Color[ColorCount];

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            byte[] data = stream.ReadBytes(ColorCount * 3);
            for (int i = 0; i < ColorCount; i++)
            {
                int offset = i * 3;
                Colors[i] = Color.FromArgb(data[offset], data[offset + 1], data[offset + 2]);
            }
        }
    }
}
