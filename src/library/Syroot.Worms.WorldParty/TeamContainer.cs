using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.WorldParty
{
    /// <summary>
    /// Represents the list of teams and unlocked game features stored in WGT files.
    /// Used by WWP. See https://worms2d.info/File_formats.
    /// </summary>
    public class TeamContainer : ILoadableFile, ISaveableFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _signature = 0x00505757; // "WWP\0"

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamContainer"/> class.
        /// </summary>
        public TeamContainer() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamContainer"/> class, loading the data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public TeamContainer(Stream stream) => Load(stream);

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamContainer"/> class, loading the data from the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public TeamContainer(string fileName) => Load(fileName);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value possibly indicating a version of the file format.
        /// </summary>
        public byte Version { get; set; }

        public byte Unknown1 { get; set; }

        public byte Unknown2 { get; set; }

        /// <summary>
        /// Gets or sets 840 unknown bytes, all possibly 0.
        /// </summary>
        public byte[] Unknown3 { get; set; } = new byte[840];

        /// <summary>
        /// Gets or sets the list of <see cref="Team"/> instances stored.
        /// </summary>
        public IList<Team> Teams { get; set; } = new List<Team>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Read the header.
            if (reader.ReadUInt32() != _signature)
                throw new InvalidDataException("Invalid WWP file signature.");
            Version = reader.Read1Byte(); // Really version?

            // Read global settings.
            byte teamCount = reader.Read1Byte();
            Unknown1 = reader.Read1Byte();
            Unknown2 = reader.Read1Byte();
            Unknown3 = reader.ReadBytes(840);

            // Read the teams.
            Teams = new List<Team>(teamCount);
            while (teamCount-- > 0)
                Teams.Add(reader.Load<Team>());
        }

        /// <inheritdoc/>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Write the header.
            writer.Write(_signature);
            writer.Write(Version);

            // Write global settings.
            writer.Write((byte)Teams.Count);
            writer.Write(Unknown1);
            writer.Write(Unknown2);
            writer.Write(Unknown3);

            // Write the teams.
            foreach (Team team in Teams)
                team.Save(writer.BaseStream);
        }

        /// <inheritdoc/>
        public void Save(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Save(stream);
        }
    }
}
