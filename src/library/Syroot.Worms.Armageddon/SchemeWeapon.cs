using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents the configuration of a weapon in a <see cref="Scheme"/>. 
    /// </summary>
    [DebuggerDisplay("Ammo={Ammo} Power={Power} Delay={Delay} Crates={Crates}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct SchemeWeapon : IEquatable<SchemeWeapon>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>Amount with which a team is equipped at game start. 10 and negative values represent infinity.</summary>
        public sbyte Ammo;
        /// <summary>Intensity of this weapon.</summary>
        public byte Power;
        /// <summary>Number of turns required to be taken by each team before this weapon becomes available. Negative
        /// values represent infinity.</summary>
        public sbyte Delay;
        /// <summary>Probability of this weapon to appear in crates. Has no effect on super weapons.</summary>
        public sbyte Prob;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns whether two <see cref="SchemeWeapon"/> instances are equal by value.
        /// </summary>
        /// <param name="left">The first instance to compare.</param>
        /// <param name="right">The second instance to compare.</param>
        /// <returns>Whether the instances are equal.</returns>
        public static bool operator ==(SchemeWeapon left, SchemeWeapon right) => left.Equals(right);

        /// <summary>
        /// Returns whether two <see cref="SchemeWeapon"/> instances are inequal by value.
        /// </summary>
        /// <param name="left">The first instance to compare.</param>
        /// <param name="right">The second instance to compare.</param>
        /// <returns>Whether the instances are inequal.</returns>
        public static bool operator !=(SchemeWeapon left, SchemeWeapon right) => !(left == right);

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override bool Equals(object obj) => obj is SchemeWeapon weapon && Equals(weapon);

        /// <inheritdoc/>
        public bool Equals(SchemeWeapon other)
            => Ammo == other.Ammo
            && Power == other.Power
            && Delay == other.Delay
            && Prob == other.Prob;

        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(Ammo, Power, Delay, Prob);
    }
}
