﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Syroot.Worms.Armageddon
{
    partial class Scheme
    {
        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        /// <summary>
        /// Represents a list of <see cref="SchemeWeapon"/> instances which can be accessed by <see cref="Weapon"/>.
        /// </summary>
        public class WeaponList : IReadOnlyCollection<SchemeWeapon>
        {
            [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
            private readonly SchemeWeapon[] _array = new SchemeWeapon[64];

            /// <summary>
            /// Gets a reference to the weapon with the given <paramref name="index"/>.
            /// </summary>
            /// <param name="index">The index of the weapon to access.</param>
            /// <returns>A reference to the <see cref="SchemeWeapon"/>.</returns>
            public ref SchemeWeapon this[int index] => ref _array[index];

            /// <summary>
            /// Gets a reference to the weapon with the given <paramref name="name"/>.
            /// </summary>
            /// <param name="name">The <see cref="Weapon"/> of the weapon to access.</param>
            /// <returns>A reference to the <see cref="SchemeWeapon"/>.</returns>
            public ref SchemeWeapon this[Weapon name] => ref _array[(int)name];

            /// <inheritdoc/>
            public int Count => _array.Length;

            /// <inheritdoc/>
            public IEnumerator<SchemeWeapon> GetEnumerator() => ((IEnumerable<SchemeWeapon>)_array).GetEnumerator();

            internal Span<SchemeWeapon> AsSpan(int length) => _array.AsSpan(0, length);

            /// <inheritdoc/>
            IEnumerator IEnumerable.GetEnumerator() => _array.GetEnumerator();
        }
    }
}
