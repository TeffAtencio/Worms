using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class ProdStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Damage { get; set; }

        [BinaryMember(Order = 2)] public int Force { get; set; }

        [BinaryMember(Order = 3)] public int Angle { get; set; }
    }
}