using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class LauncherStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int SpriteSize { get; set; }

        [BinaryMember(Order = 2)] public int FixedSpeed { get; set; }

        [BinaryMember(Order = 3, BooleanCoding = BooleanCoding.Dword)] public bool RunAway { get; set; }

        [BinaryMember(Order = 4)] public CollisionFlags Collisions { get; set; }

        [BinaryMember(Order = 5)] public int ExplosionBias { get; set; }

        [BinaryMember(Order = 6)] public int ExplosionPushPower { get; set; }

        [BinaryMember(Order = 7)] public int ExplosionDamage { get; set; }

        [BinaryMember(Order = 8)] public int ExplosionDamageVariation { get; set; }

        [BinaryMember(Order = 9)] public int ExplosionUnknown { get; set; }

        [BinaryMember(Order = 10)] public Sprite Sprite { get; set; }

        [BinaryMember(Order = 11)] public int VariableSpeed { get; set; }

        [BinaryMember(Order = 12)] public int WindFactor { get; set; }

        [BinaryMember(Order = 13)] public int MotionRandomness { get; set; }

        [BinaryMember(Order = 14)] public int GravityFactor { get; set; }

        [BinaryMember(Order = 15)] public int ExplosionCountdown { get; set; }

        [BinaryMember(Order = 16)] public int ExplosionTimer { get; set; }

        [BinaryMember(Order = 17)] public Sound Sound { get; set; }

        [BinaryMember(Order = 18, BooleanCoding = BooleanCoding.Dword)] public bool ExplodeOnSpace { get; set; }

        [BinaryMember(Order = 19)] public ExplosionAction ExplosionAction { get; set; }

        [BinaryMember(Order = 20, Converter = typeof(ActionConverter))] public IAction? Action { get; set; }

        [BinaryMember(Order = 21, OffsetOrigin = OffsetOrigin.Begin, Offset = 180)]
        public ExplosionTarget ExplosionTarget { get; set; }

        [BinaryMember(Order = 22, Offset = sizeof(int), Converter = typeof(TargetConverter))]
        public ITarget? Target { get; set; }
    }

    public enum ExplosionAction : int
    {
        None,
        Home,
        Bounce,
        Roam,
        Dig
    }

    public enum ExplosionTarget : int
    {
        None,
        Clusters,
        Fire
    }
}