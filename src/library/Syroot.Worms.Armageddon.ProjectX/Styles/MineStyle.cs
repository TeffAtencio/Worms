using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class MineStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public Mine Mine;
    }
}