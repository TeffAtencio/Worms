using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class KamikazeStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int FlyingTime { get; set; }

        [BinaryMember(Order = 2)] public int ExplosionDamage { get; set; }

        [BinaryMember(Order = 3)] public int FireSound { get; set; }

        [BinaryMember(Order = 4)] public int Damage { get; set; }

        [BinaryMember(Order = 5)] public int ImpactForce { get; set; }

        [BinaryMember(Order = 6)] public int ImpactAngle { get; set; }
    }
}