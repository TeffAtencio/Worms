using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class SuicideBomberStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int DiseasePoints { get; set; }

        [BinaryMember(Order = 2)] public int Damage { get; set; }
    }
}