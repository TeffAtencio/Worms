using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class FirepunchStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Damage { get; set; }

        [BinaryMember(Order = 2)] public int Angle { get; set; }

        [BinaryMember(Order = 3)] public int PushPower { get; set; }

        [BinaryMember(Order = 4)] public int JumpHeight { get; set; }
    }
}