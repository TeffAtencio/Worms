using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class BowStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Damage { get; set; }
    }
}