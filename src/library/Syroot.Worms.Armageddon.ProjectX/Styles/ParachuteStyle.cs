using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class ParachuteStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int WindResponse { get; set; }
    }
}