using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class NuclearTestStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int WaterRise { get; set; }

        [BinaryMember(Order = 2)] public int DiseasePoints { get; set; }
    }
}