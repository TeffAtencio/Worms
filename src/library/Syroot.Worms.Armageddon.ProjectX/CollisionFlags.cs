using System;

namespace Syroot.Worms.Armageddon.ProjectX
{
    [Flags]
    public enum CollisionFlags : int
    {
        None,
        Unused0 = 1 << 0,
        Terrain = 1 << 1,
        WormsOnTerrain = 1 << 2,
        WormsUsingWeapon = 1 << 3,
        WormsInAir = 1 << 4,
        WormsOnRope = 1 << 5,
        FrozenWorms = 1 << 6,
        Unused7 = 1 << 7,
        KamikazeBomber = 1 << 8,
        GasCanisters = 1 << 9,
        Mines = 1 << 10,
        Crates = 1 << 11,
        DonorCards = 1 << 12,
        Gravestones = 1 << 13,
        Unused14 = 1 << 14,
        OtherWeapons = 1 << 15,
        LongbowArrows = 1 << 16,
        OilDrums = 1 << 17,
        Unused18 = 1 << 18,
        Unused19 = 1 << 19,
        Unused20 = 1 << 20,
        Unused21 = 1 << 21,
        Skimming = 1 << 22,
        Unused23 = 1 << 23,
        Unused24 = 1 << 24,
        Unused25 = 1 << 25,
        Unused26 = 1 << 26,
        Unused27 = 1 << 27,
        Unused28 = 1 << 28,
        Unused29 = 1 << 29,
        Unknown = 1 << 30,
        Unused31 = 1 << 31
    }
}
