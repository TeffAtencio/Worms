using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public struct Sound
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public ushort SoundIndex;
        [BinaryMember(Order = 2, BooleanCoding = BooleanCoding.Word)] public bool RepeatSound;
        [BinaryMember(Order = 3, BooleanCoding = BooleanCoding.Dword)] public bool UseExplosionSound;
        [BinaryMember(Order = 4)] public int SoundBeforeExplosion;
        [BinaryMember(Order = 5)] public int DelayBeforeExplosion;
    }
}
