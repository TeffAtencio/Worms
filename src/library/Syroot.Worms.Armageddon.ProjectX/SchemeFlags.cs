using System.Runtime.InteropServices;

namespace Syroot.Worms.Armageddon.ProjectX
{
    /// <summary>
    /// Represents global Project X scheme flags affecting general game behavior.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SchemeFlags
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public bool CyclicMaps;
        public bool UnderwaterJetpack;
        public bool UnderwaterRope;
        public bool Unused1;
        public bool Unused2;
        public bool Unused3;
        public bool Unused4;
        public bool Unused5;
        public bool Unused6;
        public bool Unused7;
        public bool Unused8;
        public bool ShotDoesntEndTurn;
        public bool LoseControlDoesntEndTurn;
        public bool FiringPausesTimer;
        public bool EnableSchemePowers;
    }
}
