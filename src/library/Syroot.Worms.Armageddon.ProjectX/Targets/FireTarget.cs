using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class FireTarget : ITarget
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Power { get; set; }

        [BinaryMember(Order = 2)] public int Spread { get; set; }

        [BinaryMember(Order = 3)] public int Time { get; set; }

        [BinaryMember(Order = 4, BooleanCoding = BooleanCoding.Dword)] public bool StayBetweenTurns { get; set; }
    }
}
