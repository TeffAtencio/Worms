#include "wkIni.h"
#include <stdio.h>

namespace wk
{
	Ini::Ini(LPCSTR fileName)
	{
		GetModuleFileName(NULL, _filePath, MAX_PATH);
		char* sepIdx = strrchr(_filePath, '\\') + 1;
		strcpy_s(sepIdx, MAX_PATH - (int)(sepIdx - _filePath), fileName);
	}

	void Ini::get(LPCSTR category, LPCSTR key, BOOL& result, UINT fallback) const
	{
		result = GetPrivateProfileInt(category, key, fallback, _filePath);
	}

	void Ini::get(LPCSTR category, LPCSTR key, UINT& result, UINT fallback) const
	{
		result = GetPrivateProfileInt(category, key, fallback, _filePath);
	}

	void Ini::get(LPCSTR category, LPCSTR key, LPSTR result, INT resultLength, LPCSTR fallback) const
	{
		GetPrivateProfileString(category, key, fallback, result, resultLength, _filePath);
	}

	void Ini::set(LPCSTR category, LPCSTR key, UINT value) const
	{
		CHAR buffer[32];
		sprintf_s(buffer, "%d", value);
		WritePrivateProfileString(category, key, buffer, _filePath);
	}

	void Ini::set(LPCSTR category, LPCSTR key, LPCSTR value) const
	{
		WritePrivateProfileString(category, key, value, _filePath);
	}
}