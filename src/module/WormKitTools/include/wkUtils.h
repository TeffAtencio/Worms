#pragma once
#include <stdexcept>
#include <Windows.h>

namespace wk
{
	// 0x   00     0000       00
	//   [Game][Version][Release]
	enum GameID
	{
		GAMEID_NONE,
		
		GAMEID_W2 = 0x20000000,
		GAMEID_W2_1_05 = GAMEID_W2 | 0x010500,
		GAMEID_W2_1_05_BR = GAMEID_W2_1_05 | 0x10, // Worms2 1.05 Br
		GAMEID_W2_1_05_EN = GAMEID_W2_1_05 | 0x20, // Worms2 1.05 Du, En, Fr, It, Po, Sp, Sw
		GAMEID_W2_1_05_GE = GAMEID_W2_1_05 | 0x30, // Worms2 1.05 De
		GAMEID_W2_1_05_NA = GAMEID_W2_1_05 | 0x40, // Worms2 1.05 NA
		GAMEID_W2_1_05_SA = GAMEID_W2_1_05 | 0x50, // Worms2 1.05 SA
		GAMEID_W2_1_07 = GAMEID_W2 | 0x010700,
		GAMEID_W2_1_07_TRY = GAMEID_W2_1_07 | 0x60, // Worms2 1.07 Trymedia
		GAMEID_W2_LAST = GAMEID_W2_1_07_TRY,

		GAMEID_WA = 0x30000000,
		GAMEID_WA_3_6_31 = GAMEID_WA | 0x030630, // Worms Armageddon 3.6.31
		GAMEID_WA_3_7_2_1 = GAMEID_WA | 0x030721, // Worms Armageddon 3.7.2.1
		GAMEID_WA_3_8 = GAMEID_WA | 0x030800,
		GAMEID_WA_3_8_CD = GAMEID_WA_3_8 | 0x10, // Worms Armageddon 3.8 CD
		GAMEID_WA_LAST = GAMEID_WA_3_8_CD,

		GAMEID_WWP = 0x40000000,
		GAMEID_WWP_LAST = GAMEID_WWP,

		GAMEID_OW = 0x50000000,
		GAMEID_OW_LAST = GAMEID_OW,

		GAMEID_WWPA = 0x60000000,
		GAMEID_WWPA_LAST = GAMEID_WWPA,
	};

	int getGameID(DWORD timeDateStamp);
	std::string getErrorMessage(int error);
}
