﻿using System.Net;

namespace Syroot.Worms.Mgame.GameServer
{
    internal class Config
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private IPAddress _ipAddress;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the IP sent to clients to connect to. Defaults to retrieving the external address.
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// Gets or sets the port under which the server listens for new connections.
        /// </summary>
        public ushort Port { get; set; }

        public string Name { get; set; }
        public string Region { get; set; }
        public ushort Version { get; set; }

        public int SendDelay { get; set; }

        internal IPAddress IPAddress
        {
            get
            {
                // Retrieve external IP if not yet done and given IP is invalid.
                if (_ipAddress == null && (IP == null || !IPAddress.TryParse(IP, out _ipAddress)))
                {
                    using WebClient webClient = new WebClient();
                    _ipAddress = IPAddress.Parse(webClient.DownloadString("https://ip.syroot.com"));
                }
                return _ipAddress;
            }
        }

        internal IPEndPoint EndPoint => new IPEndPoint(IPAddress, Port);
    }
}
