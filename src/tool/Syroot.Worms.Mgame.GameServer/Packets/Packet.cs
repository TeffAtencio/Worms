﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets
{
    /// <summary>
    /// Represents a packet with an ID specifying its contents. To allow the server to instantiate child packet classes,
    /// decorate it with the <see cref="PacketAttribute"/>.
    /// </summary>
    internal interface IPacket
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        void Load(ref SpanReader reader);

        void Save(ref SpanWriter writer);
    }
}
