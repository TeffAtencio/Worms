﻿using System;
using System.IO;
using System.Net.Sockets;
using Syroot.Worms.Mgame.GameServer.Core.IO;
using Syroot.Worms.Mgame.GameServer.Core.Net;
using Syroot.Worms.Mgame.GameServer.Core.Reflection;

namespace Syroot.Worms.Mgame.GameServer.Packets
{
    /// <summary>
    /// Represents a class capable of dispatching received <see cref="IPacket"/> instances to a corresponding method.
    /// </summary>
    internal class PacketConnection : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _maxCacheSize = 2048;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly MethodHandler<IPacket> _methodHandler;
        private readonly SafeNetworkStream _tcpStream;
        private readonly IPacketFormat[] _packetFormatPipe;
        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PacketConnection"/> class, handling the given
        /// <paramref name="tcpClient"/>.
        /// </summary>
        /// <param name="tcpClient">The <see cref="System.Net.Sockets.TcpClient"/> to communicate with.</param>
        /// <param name="packetFormatPipe">The array of <see cref="IPacketFormat"/> instances which are called in order
        /// to handle a packet.</param>
        internal PacketConnection(TcpClient tcpClient, params IPacketFormat[] packetFormatPipe)
        {
            TcpClient = tcpClient;
            _packetFormatPipe = packetFormatPipe;
            _tcpStream = TcpClient.GetSafeStream();
            _methodHandler = new MethodHandler<IPacket>(this);
        }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        protected event EventHandler<IPacket> PrePacketHandle;
        protected event EventHandler<IPacket> PrePacketSend;
        protected event EventHandler<Exception> UnhandledException;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the <see cref="TcpClient"/> with which the connection communicates.
        /// </summary>
        internal TcpClient TcpClient { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Starts handling packets incoming from this connection and dispatches them to corresponding methods.
        /// </summary>
        internal void Listen()
        {
            try
            {
                IPacket packet;
                while ((packet = ReceivePacket()) != null)
                {
                    PrePacketHandle?.Invoke(this, packet);
                    _methodHandler.Handle(packet);
                }
            }
            catch (Exception ex)
            {
                UnhandledException?.Invoke(this, ex);
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                    TcpClient.Dispose();
                _tcpStream.Dispose();
                _disposed = true;
            }
        }

        protected void SendPacket(IPacket packet)
        {
            PrePacketSend?.Invoke(this, packet);
            try
            {
                // Let each packet format try to serialize the data.
                foreach (IPacketFormat format in _packetFormatPipe)
                    if (format.TrySave(_tcpStream, packet))
                        return;
                throw new NotImplementedException("Cannot send unhandled packet format.");
            }
            catch (IOException) { } // A network error appeared, and communication should end.
            catch (ObjectDisposedException) { } // The underlying stream was most apparently closed.
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private IPacket ReceivePacket()
        {
            try
            {
                // Let each packet format read from the stream without consuming data.
                using (var cachedStream = new CachedStream<SafeNetworkStream>(_tcpStream, _maxCacheSize, true))
                {
                    foreach (IPacketFormat format in _packetFormatPipe)
                    {
                        IPacket packet = format.TryLoad(cachedStream);
                        if (packet != null)
                            return packet;
                        cachedStream.MoveToCacheStart();
                    }
                }
                throw new NotImplementedException("Cannot receive unhandled packet format.");
            }
            catch (IOException) { return null; } // The underlying socket closed or sent invalid data.
            catch (ObjectDisposedException) { return null; } // The underlying stream closed.
        }
    }
}
