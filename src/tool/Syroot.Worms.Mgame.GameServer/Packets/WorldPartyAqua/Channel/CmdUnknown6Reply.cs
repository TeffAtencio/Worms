﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="CmdUnknown6Query"/>.
    /// </summary>
    [WwpaPacket(0x10B, Command = 6)]
    internal class CmdUnknown6Reply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Unknown6Status Result { get; set; }

        public ushort Port { get; set; } // Unknown2 = x + 6, Unknown3 = x

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteEnumSafe(Result);
            if (Result != Unknown6Status.Disconnect)
                writer.WriteUInt16(Port);
        }
    }

    internal enum Unknown6Status : int
    {
        Unknown2 = 2,
        Unknown3 = 3,
        Disconnect = 4
    }
}
