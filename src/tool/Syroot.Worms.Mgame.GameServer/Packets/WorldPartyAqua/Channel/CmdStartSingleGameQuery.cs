﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the <see cref="CmdQuery"/> client request for a <see cref="CmdStartSingleGameReply"/>.
    /// </summary>
    [WwpaPacket(0x10A, Command = 5)]
    internal class CmdStartSingleGameQuery : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) { }

        public void Save(ref SpanWriter writer) { }
    }
}
