﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to an <see cref="AddFriendQuery"/>.
    /// </summary>
    [WwpaPacket(0x15D)]
    internal class AddFriendReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public FriendRequestResult Result { get; set; }

        /// <summary>
        /// Gets or sets the name of the user who was added as a friend.
        /// </summary>
        public string UserName { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteEnumSafe(Result);
            if (Result == FriendRequestResult.Success)
                writer.WriteString2(UserName);
        }
    }

    internal enum FriendRequestResult : int
    {
        Success = 0,
        NotFound = 1,
        Fail = 99
    }
}
