﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Data;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="CmdFindUserQuery"/>.
    /// </summary>
    [WwpaPacket(0x10B, Command = 1)]
    internal class CmdFindUserReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool Success { get; set; }

        public string NickName { get; set; }

        public UserGender Gender { get; set; }

        public int GamesWon { get; set; }

        public int GamesLost { get; set; }

        public int GamesDrawn { get; set; }

        public ushort Level { get; set; }

        public uint MaybeClass { get; set; }

        public string Guild { get; set; }

        public long Experience { get; set; }

        public long Currency { get; set; }

        public uint MaybeLocation { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public DateTime Birthday { get; set; }

        public string Introduction { get; set; }

        public IList<int> MaybeAvatar { get; set; } // 13 elements

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteInt32(Success ? 1 : 2);
            if (Success)
            {
                writer.WriteString2(NickName);
                writer.WriteEnumSafe(Gender);
                writer.WriteInt32(GamesWon);
                writer.WriteInt32(GamesLost);
                writer.WriteInt32(GamesDrawn);
                writer.WriteUInt16(Level);
                writer.WriteUInt32(MaybeClass);
                writer.WriteString2(Guild);
                writer.WriteInt64(Experience);
                writer.WriteInt64(Currency);
                writer.WriteString2(Phone);
                writer.WriteString2(Address);
                writer.WriteString2(Birthday.ToShortDateString());
                writer.WriteString2(Introduction);
                writer.WriteUInt32(MaybeLocation);
                for (int i = 0; i < 13; i++)
                    writer.WriteInt32(MaybeAvatar[i]);
            }
        }
    }
}
