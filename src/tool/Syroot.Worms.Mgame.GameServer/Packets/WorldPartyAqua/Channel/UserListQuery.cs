﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="UserListReply"/>.
    /// </summary>
    [WwpaPacket(0x128)]
    internal class UserListQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ushort UnknownA { get; set; }

        public UserListCommand Command { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UnknownA = reader.ReadUInt16();
            Command = reader.ReadEnumSafe<UserListCommand>();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }

    public enum UserListCommand : ushort
    {
        Unknown0 = 0,
        Refresh = 1,
        NextPage = 2,
        PreviousPage = 3,
        Unknown4 = 4
    }
}
