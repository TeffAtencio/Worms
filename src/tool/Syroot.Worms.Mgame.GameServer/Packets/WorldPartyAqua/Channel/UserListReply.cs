﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Data;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="UserListQuery"/>.
    /// </summary>
    [WwpaPacket(0x129)]
    internal class UserListReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public UserListCommand Command { get; set; }

        public IList<UserListPlayer> Players { get; set; } // max. 10 elements handled

        public IList<UserListUnknownElse> UnknownElses { get; set; } // max. 6 elements handled

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteEnumSafe(Command);
            switch (Command)
            {
                case UserListCommand.Unknown0:
                    for (int i = 0; i < 10; i++)
                    {
                        if (i < Players.Count)
                        {
                            writer.WriteByte(1);
                            UserListPlayer player = Players[i];
                            writer.WriteString2(player.UserName);
                            writer.WriteEnumSafe(player.Gender);
                            writer.WriteInt32(player.Guild);
                            writer.WriteUInt16(player.Level);
                        }
                        else
                        {
                            writer.WriteByte(2);
                        }
                    }
                    break;
                default:
                    for (int i = 0; i < 6; i++)
                    {
                        if (i < UnknownElses.Count)
                        {
                            writer.WriteByte(1);
                            UserListUnknownElse unknownElse = UnknownElses[i];
                            writer.WriteUInt16(unknownElse.UnknownZ);
                            writer.WriteString(unknownElse.Text);
                            writer.WriteUInt16(unknownElse.UnknownY);
                            writer.WriteBytes(unknownElse.UnknownX);

                            writer.WriteByte((byte)unknownElse.Subs.Count);
                            foreach (UserListUnknownElseSub sub in unknownElse.Subs)
                            {
                                writer.WriteString2(sub.Text);
                                writer.WriteByte(sub.UnknownA);
                                writer.WriteInt32(sub.UnknownB);
                                writer.WriteUInt16(sub.UnknownC);
                            }
                        }
                        else
                        {
                            writer.WriteByte(2);
                        }
                    }
                    break;
            }
        }
    }

    internal class UserListPlayer
    {
        public string UserName { get; set; }
        public UserGender Gender { get; set; }
        public int Guild { get; set; }
        public ushort Level { get; set; }
    }

    internal class UserListUnknownElse
    {
        public ushort UnknownZ { get; set; }
        public string Text { get; set; }
        public ushort UnknownY { get; set; }
        public byte[] UnknownX { get; set; } // 20 bytes
        public IList<UserListUnknownElseSub> Subs { get; set; }
    }

    internal class UserListUnknownElseSub
    {
        public string Text { get; set; }
        public byte UnknownA { get; set; }
        public int UnknownB { get; set; }
        public ushort UnknownC { get; set; }
    }
}
