﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="Unknown11FReply"/>.
    /// </summary>
    [WwpaPacket(0x11E)]
    internal class Unknown11EQuery : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) { }

        public void Save(ref SpanWriter writer) { }
    }
}
