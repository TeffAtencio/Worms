﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the server response to a <see cref="LoginQuery"/>.
    /// </summary>
    [WwpaPacket(0x801A)]
    internal class LoginReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public LoginResult Result { get; set; }

        public byte UnknownA { get; set; }

        public ushort UnknownB { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteEnumSafe(Result);
            if (Result == LoginResult.Success)
            {
                writer.WriteByte(UnknownA);
                writer.WriteUInt16(UnknownB);
            }
        }
    }

    internal enum LoginResult : byte
    {
        Success = 0,
        SaveInfoOnly = 12,
        NicknameDoesNotExist = 14,
        LoginFailed = 15,
        FailedToFindUser = 99
    }
}
