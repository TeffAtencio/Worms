﻿using System;
using System.Net;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents the server response to a <see cref="ChannelEnterQuery"/>.
    /// </summary>
    [OWServerPacket(0x8035)]
    internal class ChannelEnterReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the <see cref="IPEndPoint"/> to which the game client connects to enter the channel.
        /// </summary>
        public IPEndPoint EndPoint { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteString2(EndPoint.Address.ToString());
            writer.WriteUInt16((ushort)EndPoint.Port);
        }
    }
}
