﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents the server response to a <see cref="LoginQuery"/>.
    /// </summary>
    [OWServerPacket(0x8001)]
    internal class LoginReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal LoginResult Result { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user can send any messages. Apparently also affects possibility
        /// of even joining a channel.
        /// </summary>
        internal bool HasMessageRights { get; set; } = true;

        internal LoginPlayerInfo[] PlayerInfos { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            bool loginSuccessful = Result == LoginResult.Success;
            writer.WriteBoolean(loginSuccessful);
            if (loginSuccessful)
            {
                writer.WriteBoolean2(!HasMessageRights);
                writer.WriteUInt16((ushort)PlayerInfos.Length);
                foreach (LoginPlayerInfo playerInfo in PlayerInfos)
                {
                    writer.WriteString2(playerInfo.ID);
                    writer.WriteUInt16(playerInfo.Rank);
                }
            }
            else
            {
                writer.WriteEnumSafe(Result);
            }
        }
    }

    internal class LoginPlayerInfo
    {
        internal string ID { get; set; }
        internal ushort Rank { get; set; }
    }

    internal enum LoginResult : byte
    {
        Success = 0,
        IDAlreadyInUse = 3,
        UnverifiedID = 4,
        IncorrectID = 6,
        IncorrectPassword = 7,
        DuplicateIDs = 9,
        IncorrectVersion = 10,
        BannedID = 11,
        PublicAccessBanned = 12,
        TemporarilyBannedID = 13
    }
}
