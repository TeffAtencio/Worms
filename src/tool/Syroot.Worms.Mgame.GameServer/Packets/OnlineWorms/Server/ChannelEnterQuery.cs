﻿using System;
using System.Net;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents the client request for a <see cref="ChannelEnterReply"/>.
    /// </summary>
    [OWServerPacket(0x8034)]
    internal class ChannelEnterQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string PlayerID { get; set; }

        public IPEndPoint ChannelEndPoint { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            PlayerID = reader.ReadString2();
            ChannelEndPoint = new IPEndPoint(IPAddress.Parse(reader.ReadString2()), reader.ReadUInt16());
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
