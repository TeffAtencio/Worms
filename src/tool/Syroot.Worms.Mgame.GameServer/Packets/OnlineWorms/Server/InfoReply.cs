﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents an additional server response to a <see cref="LoginQuery"/>, providing informational server
    /// screen text.
    /// </summary>
    [OWServerPacket(0x8033)]
    internal class InfoReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the text to display in the information box. Only the first 10 lines are read by the server.
        /// </summary>
        internal string Text { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteString2(Text.Replace(Environment.NewLine, "\n"));
        }
    }
}
