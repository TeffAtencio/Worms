﻿using System;
using System.Net;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Packets.Data;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents the client request for a <see cref="LoginReply"/>.
    /// </summary>
    [OWServerPacket(0x8000)]
    internal class LoginQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal ushort Unknown1 { get; set; }

        internal PlayerCredentials[] Players { get; set; }

        internal IPAddress ClientIP { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            Unknown1 = reader.ReadUInt16();
            Players = new PlayerCredentials[reader.ReadUInt16()];
            for (int i = 0; i < Players.Length; i++)
                Players[i] = reader.ReadCredentials();
            ClientIP = IPAddress.Parse(reader.ReadString2());
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
