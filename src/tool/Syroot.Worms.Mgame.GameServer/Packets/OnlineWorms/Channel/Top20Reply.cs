﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="Top20Query"/>.
    /// </summary>
    [OWChannelPacket(0x36)]
    internal class Top20Reply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string UnknownA { get; set; } // Max. 30 chars

        public IList<ChannelTop20Player> Top20 { get; set; } // 20 elements.

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteStringFix(UnknownA, 30);
            foreach (ChannelTop20Player top20Player in Top20)
            {
                writer.WriteUInt16(top20Player.Rank);
                writer.WriteStringFix(top20Player.Name, 12);
                writer.WriteUInt64(top20Player.Experience);
            }
        }
    }

    /// <summary>
    /// Represents a player channel rank, apparently.
    /// </summary>
    public class ChannelTop20Player
    {
        public ushort Rank { get; set; }
        public string Name { get; set; } // Max. 12 chars
        public ulong Experience { get; set; }
    }
}
