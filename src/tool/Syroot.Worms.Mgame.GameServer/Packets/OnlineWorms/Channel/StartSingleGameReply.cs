﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="StartSingleGameQuery"/>.
    /// </summary>
    [OWChannelPacket(0x39)]
    internal class StartSingleGameReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool Success { get; set; } = true;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteBoolean2(Success);
        }
    }
}
