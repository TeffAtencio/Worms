﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="StartSingleGameReply"/>.
    /// </summary>
    [OWChannelPacket(0x38)]
    internal class StartSingleGameQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public GameStartRoundType RoundType { get; set; }
        public byte UnknownB { get; set; }
        public byte UnknownC { get; set; } // 0x3D on first round, 0x3E on any other

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            RoundType = reader.ReadEnumSafe<GameStartRoundType>();
            UnknownB = reader.ReadByte();
            UnknownC = reader.ReadByte();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }

    internal enum GameStartRoundType : byte
    {
        First = 1,
        Next = 2
    }
}
