﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syroot.BinaryData;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.Reflection;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms
{
    /// <summary>
    /// Represents the factory of packets in the OW channel packet format.
    /// </summary>
    internal class OWChannelPacketFormat : IPacketFormat
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const ushort _tagStart = 0xFFFE;
        private const ushort _tagEnd = 0xFEFF;
        private const int _maxDataSize = 1024;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly IList<(Type type, OWChannelPacketAttribute attrib)> _cachedClasses
             = ReflectionTools.GetAttributedClasses<OWChannelPacketAttribute>();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        private OWChannelPacketFormat() { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal static OWChannelPacketFormat Instance { get; } = new OWChannelPacketFormat();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public IPacket TryLoad(Stream stream)
        {
            // Read head.
            if (stream.ReadUInt16() != _tagStart || stream.Read1Byte() != 1)
                return null;
            int dataSize = stream.ReadUInt16();
            IPacket packet = GetPacket(stream.Read1Byte());
            if (packet == null)
                return null;

            // Read data.
            SpanReader reader = new SpanReader(stream.ReadBytes(dataSize), encoding: Encodings.Korean);

            // Read tail.
            if (stream.ReadUInt16() != _tagEnd)
                return null;

            // Deserialize and return packet.
            packet.Load(ref reader);
            return packet;
        }

        public bool TrySave(Stream stream, IPacket packet)
        {
            byte? id = GetID(packet);
            if (!id.HasValue)
                return false;

            // Retrieve data. Must have at least 1 byte.
            SpanWriter writer = new SpanWriter(new byte[_maxDataSize], encoding: Encodings.Korean);
            packet.Save(ref writer);
            writer.Position = Math.Max(1, writer.Position);

            // Send head.
            stream.WriteUInt16(_tagStart);
            stream.WriteByte(1);
            stream.WriteUInt16((ushort)writer.Position);
            stream.WriteByte(id.Value);

            // Send data.
            stream.Write(writer.Span);

            // Send tail.
            stream.WriteUInt16(_tagEnd);
            return true;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private IPacket GetPacket(byte id)
        {
            Type packetType = _cachedClasses.Where(x => x.attrib.ID == id).FirstOrDefault().type;
            return packetType == null ? null : (IPacket)Activator.CreateInstance(packetType);
        }

        private byte? GetID(IPacket packet)
        {
            Type packetType = packet.GetType();
            return _cachedClasses.Where(x => x.type == packetType).FirstOrDefault().attrib?.ID;
        }
    }
}
