﻿using System.IO;

namespace Syroot.Worms.Mgame.GameServer.Core.IO
{
    /// <summary>
    /// Represents a <see cref="Stream"/> wrapping around another, implementing default methods.
    /// </summary>
    /// <typeparam name="T">The type of the wrapped <see cref="Stream"/>.</typeparam>
    internal class WrapperStream<T> : Stream
        where T : Stream
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly bool _leaveOpen;
        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="WrapperStream{T}"/> class wrapping the given
        /// <paramref name="baseStream"/> which it may <paramref name="leaveOpen"/> on dispose.
        /// </summary>
        /// <param name="baseStream">The <see cref="Stream"/> to wrap.</param>
        /// <param name="leaveOpen"><see langword="true"/> to leave the stream open on <see cref="Dispose()"/>.</param>
        internal WrapperStream(T baseStream, bool leaveOpen = false)
        {
            BaseStream = baseStream;
            _leaveOpen = leaveOpen;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public override bool CanRead => BaseStream.CanRead;
        public override bool CanSeek => BaseStream.CanSeek;
        public override bool CanWrite => BaseStream.CanWrite;
        public override long Length => BaseStream.Length;
        public override long Position
        {
            get => BaseStream.Position;
            set => BaseStream.Position = value;
        }

        /// <summary>
        /// Gets the underlying stream of type <see cref="T"/>.
        /// </summary>
        internal T BaseStream { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void Flush() => BaseStream.Flush();
        public override int Read(byte[] buffer, int offset, int count) => BaseStream.Read(buffer, offset, count);
        public override long Seek(long offset, SeekOrigin origin) => BaseStream.Seek(offset, origin);
        public override void SetLength(long value) => BaseStream.SetLength(value);
        public override void Write(byte[] buffer, int offset, int count) => BaseStream.Write(buffer, offset, count);

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing && !_leaveOpen)
                BaseStream.Dispose();
            _disposed = true;
            base.Dispose(disposing);
        }
    }
}
