﻿using System;
using System.IO;

namespace Syroot.Worms.Mgame.GameServer.Core.IO
{
    /// <summary>
    /// Represents a <see cref="Stream"/> which caches the last bytes in a buffer and allows virtual back-seeking even
    /// in non-seekable streams.
    /// </summary>
    /// <typeparam name="T">The type of the wrapped <see cref="Stream"/>.</typeparam>
    internal class CachedStream<T> : WrapperStream<T>
        where T : Stream
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly byte[] _cache;
        private int _virtualPosition;
        private int _cacheEnd;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal CachedStream(T baseStream, int cacheSize, bool leaveOpen = false) : base(baseStream, leaveOpen)
        {
            _cache = new byte[cacheSize];
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override int Read(byte[] buffer, int offset, int count)
        {
            // Return bytes from cache first, if possible.
            int cacheBytes = Math.Min(_cacheEnd - _virtualPosition, count);
            if (cacheBytes > 0)
            {
                Buffer.BlockCopy(_cache, _virtualPosition, buffer, offset, cacheBytes);
                _virtualPosition += cacheBytes;
                offset += cacheBytes;
            }

            // Read more bytes from the stream if the cache is not covering all.
            int streamBytes = count - cacheBytes;
            if (streamBytes > 0)
            {
                if (_cache.Length - _cacheEnd < streamBytes)
                    throw new IOException("Cache cannot hold more bytes.");
                streamBytes = BaseStream.Read(_cache, _cacheEnd, streamBytes);
                _cacheEnd += streamBytes;

                Buffer.BlockCopy(_cache, _virtualPosition, buffer, offset, count);
                _virtualPosition += streamBytes;
            }

            return cacheBytes + streamBytes;
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void MoveToCacheStart()
        {
            _virtualPosition = 0;
        }
    }
}
