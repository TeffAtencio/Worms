﻿using System;
using System.Diagnostics;

namespace Syroot.Worms.Mgame.GameServer
{
    /// <summary>
    /// Represents simplistic textual logging.
    /// </summary>
    internal class Log
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly object _lock = new object();

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Write(LogCategory category, string text) => Write((ConsoleColor)category, text);

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Write(ConsoleColor color, string text)
        {
            Debug.WriteLine(text);
            lock (_lock)
            {
                ConsoleColor prevColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.WriteLine(text);
                Console.ForegroundColor = prevColor;
            }
        }
    }

    internal enum LogCategory
    {
        Info = ConsoleColor.White,
        Error = ConsoleColor.Red,
        Connect = ConsoleColor.Cyan,
        Disconnect = ConsoleColor.Magenta,
        Client = ConsoleColor.DarkCyan,
        Server = ConsoleColor.DarkMagenta
    }
}
