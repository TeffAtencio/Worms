﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using SharpShell.Attributes;
using SharpShell.SharpContextMenu;

namespace Syroot.Worms.Shell
{
    /// <summary>
    /// Represents a context menu extension for <see cref="Image"/> files.
    /// </summary>
    [ComVisible(true)]
    [COMServerAssociation(AssociationType.ClassOfExtension, ".img")]
    internal class ImgContextMenu : SharpContextMenu
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override bool CanShowMenu()
        {
            // At least one image must be convertible.
            foreach (string file in SelectedItemPaths)
            {
                try
                {
                    Img img = new Img(file);
                    return true;
                }
                catch { }
            }
            return false;
        }

        protected override ContextMenuStrip CreateMenu()
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem { Text = "Convert to PNG" };
            menuItem.Click += (s, e) =>
            {
                // Convert all files.
                var tasks = new List<(string file, ConversionResult result)>();
                foreach (string file in SelectedItemPaths)
                {
                    (string file, ConversionResult result) task = (file, ConversionResult.Success);
                    tasks.Add(task);

                    // If the file already exists, do nothing.
                    string newFile = Path.ChangeExtension(Path.GetFileNameWithoutExtension(file) + " {converted}", "png");
                    if (File.Exists(newFile))
                    {
                        task.result = ConversionResult.FileExists;
                        continue;
                    }

                    // Try to convert to PNG.
                    try
                    {
                        Img img = new Img(file);
                        using (Bitmap bitmap = img.ToBitmap())
                            bitmap.Save(newFile, ImageFormat.Png);
                    }
                    catch
                    {
                        task.result = ConversionResult.InvalidImg;
                    }
                }

                // Show failed conversion results, if any.
                var failedTasks = tasks.Where(x => x.result != ConversionResult.Success).ToList();
                if (failedTasks.Count > 0)
                {
                    StringBuilder stringBuilder = new StringBuilder("Conversion of the following files failed:");
                    stringBuilder.AppendLine();
                    foreach (var (file, result) in tasks)
                        stringBuilder.AppendLine($"{file} ({GetConversionResultMessage(result)}");
                    MessageBox.Show(stringBuilder.ToString(), AssemblyInfo.Title, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            };

            ContextMenuStrip menu = new ContextMenuStrip();
            menu.Items.Add(menuItem);

            return menu;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static string GetConversionResultMessage(ConversionResult result)
        {
            switch (result)
            {
                case ConversionResult.FileExists:
                    return "file already exists";
                case ConversionResult.InvalidImg:
                    return "could not load image";
                default:
                    return null;
            }
        }
    }

    public enum ConversionResult
    {
        Success,
        InvalidImg,
        FileExists
    }
}
