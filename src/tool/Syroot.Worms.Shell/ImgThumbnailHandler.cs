﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using SharpShell.Attributes;
using SharpShell.SharpThumbnailHandler;

namespace Syroot.Worms.Shell
{
    /// <summary>
    /// Represents a thumbnail handler for <see cref="Img"/> files.
    /// </summary>
    [ComVisible(true)]
#pragma warning disable CS0618 // Type or member is obsolete, no alternative given for SharpThumbnailHandler
    [COMServerAssociation(AssociationType.FileExtension, ".img")]
#pragma warning restore CS0618
    public class ImgThumbnailHandler : SharpThumbnailHandler
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override Bitmap GetThumbnailImage(uint width)
        {
            try
            {
                Img img = new Img(SelectedItemStream);
                using (Bitmap bitmap = img.ToBitmap())
                {
                    // Calculate the thumbnail size and create the image buffer to return.
                    float scale = Math.Min(1, width / (float)bitmap.Width);

                    Size thumbnailSize = new Size((int)(scale * bitmap.Width), (int)(scale * bitmap.Height));
                    return new Bitmap(bitmap, thumbnailSize);
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
