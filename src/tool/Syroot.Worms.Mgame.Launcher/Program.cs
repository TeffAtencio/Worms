﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;

namespace Syroot.Worms.Mgame.Launcher
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main()
        {
            try
            {
                // Create and read the configuration.
                Config config = new ConfigurationBuilder()
                    .AddJsonFile("LauncherConfig.json", true)
                    .Build()
                    .Get<Config>();

                // Check which of the game executables is available.
                string executablePath = config.ExecutePathList.FirstOrDefault(x => File.Exists(x));
                if (executablePath == null)
                    throw new InvalidOperationException("Game executable was not found.");
                // Kill any possibly existing process.
                KillProcesses(Path.GetFileNameWithoutExtension(executablePath));

                // Create the launch configuration and map it.
                LaunchConfig launchConfig = CreateLaunchConfig(config);
                using (launchConfig.CreateMappedFile(config.MappingName))
                {
                    // Create and run the process.
                    using (NativeProcess process = CreateProcess(executablePath,
                        String.Join(" ", config.MappingName, config.ExecutableArgs).TrimEnd(),
                        config.StartSuspended))
                    {
                        if (config.StartSuspended)
                        {
                            if (ShowMessage(MessageBoxIcon.Information,
                                "Game process has been created and is paused. Click OK to resume or Cancel to kill it.",
                                MessageBoxButtons.OKCancel) == DialogResult.OK)
                            {
                                process.Resume();
                            }
                            else
                            {
                                process.Terminate();
                            }
                        }
                        process.WaitForExit();
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(MessageBoxIcon.Error, ex.Message, MessageBoxButtons.OK);
                Console.WriteLine(ex);
            }
        }

        private static void KillProcesses(string processName)
        {
            foreach (Process process in Process.GetProcessesByName(processName))
                process.Kill();
        }

        private static LaunchConfig CreateLaunchConfig(Config config)
        {
            LaunchConfig launchConfig = new LaunchConfig
            {
                ServerEndPoint = config.ServerEndPoint,
                UserName = config.UserName
            };
            launchConfig.SetPassword(config.Password, config.PasswordKey);
            return launchConfig;
        }

        private static NativeProcess CreateProcess(string executablePath, string commandLine, bool startSuspended)
        {
            string directory = Path.GetDirectoryName(executablePath);
            if (String.IsNullOrEmpty(directory))
                directory = null;

            NativeProcessFlags flags = NativeProcessFlags.InheritHandles;
            if (startSuspended)
                flags |= NativeProcessFlags.StartSuspended;

            return new NativeProcess(executablePath, commandLine, directory, flags);
        }

        private static DialogResult ShowMessage(MessageBoxIcon icon, string text, MessageBoxButtons buttons)
        {
            return MessageBox.Show(text, AssemblyInfo.Title, buttons, icon);
        }
    }
}
