﻿using System.Reflection;

namespace Syroot.Worms.Mgame.Launcher
{
    /// <summary>
    /// Represents entry assembly information.
    /// </summary>
    internal static class AssemblyInfo
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly Assembly _entryAssembly;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static AssemblyInfo()
        {
            _entryAssembly = Assembly.GetEntryAssembly();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the value of the <see cref="AssemblyTitleAttribute"/>.
        /// </summary>
        internal static string Title => _entryAssembly.GetCustomAttribute<AssemblyTitleAttribute>().Title;
    }
}
