using System;
using System.Drawing.Imaging;
using System.IO;
using Syroot.Worms.Mgame;

namespace Syroot.Worms.Scratchpad
{
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main()
        {
            ConvertIgdImages();
            ConvertKsfImages();
        }

        private static void ConvertIgdImages()
        {
            string userFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string igdFolder = @"C:\Games\Worms World Party Aqua\Images";
            string pngFolder = Path.Combine(userFolder, "Pictures", "IGD");
            Directory.CreateDirectory(pngFolder);

            foreach (string igdFilePath in Directory.GetFiles(igdFolder, "*.igd", SearchOption.AllDirectories))
            {
                // Load the IGD and let it convert the images.
                Igd igd = new Igd(igdFilePath);

                // Save the images in the output folder under a relative path.
                string igdFileFolder = Path.GetDirectoryName(igdFilePath);
                string relativePath = igdFileFolder.Substring(igdFolder.Length).TrimStart(Path.DirectorySeparatorChar);
                string pngIgdFolder = Path.Combine(pngFolder, relativePath, Path.GetFileName(igdFilePath));
                Directory.CreateDirectory(pngIgdFolder);
                for (int i = 0; i < igd.Images.Count; i++)
                {
                    string pngFileName = Path.ChangeExtension(i.ToString(), "png");
                    IgdImage image = igd.Images[i];
                    image.RawBitmap.ToBitmap().Save(Path.Combine(pngIgdFolder, pngFileName), ImageFormat.Png);
                }
            }
        }

        private static void ConvertKsfImages()
        {
            string userFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string ksfFolder = @"C:\Games\Online Worms\Ksf";
            string palFolder = @"C:\Games\Online Worms\Palette";
            string pngFolder = Path.Combine(userFolder, "Pictures", "KSF");
            Directory.CreateDirectory(pngFolder);
            KsfPalette fallbackPalette = new KsfPalette(Path.Combine(palFolder, "StaticImage.pal"));

            foreach (string ksfFilePath in Directory.GetFiles(ksfFolder, "*.ksf"))
            {
                // Try to find a palette for every KSF, otherwise use fallback palette.
                string ksfFileName = Path.GetFileName(ksfFilePath);
                string palFilePath = Path.Combine(palFolder, Path.ChangeExtension(ksfFileName, "pal"));
                KsfPalette palette = File.Exists(palFilePath) ? new KsfPalette(palFilePath) : fallbackPalette;

                // Load the KSF and let it convert the images.
                Ksf ksf = new Ksf(ksfFilePath, palette);

                // Save the images in the output folder.
                string pngKsfFolder = Path.Combine(pngFolder, ksfFileName);
                Directory.CreateDirectory(pngKsfFolder);
                for (int i = 0; i < ksf.Images.Count; i++)
                {
                    string pngFileName = Path.ChangeExtension(i.ToString(), "png");
                    ksf.Images[i].RawBitmap?.ToBitmap().Save(Path.Combine(pngKsfFolder, pngFileName), ImageFormat.Png);
                }
            }
        }
    }
}