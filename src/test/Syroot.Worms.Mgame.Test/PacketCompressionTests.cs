using System;
using System.Linq;
using System.Security.Cryptography;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.Worms.Mgame.Test
{
    [TestClass]
    public class PacketCompressionTests
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        // Known to cause 3 wrong bytes:
        private static readonly byte[] _origBadBytes = new byte[] { 0x68, 0x56, 0xF9, 0x1D, 0x39, 0x5F, 0xD0, 0x57, 0x13, 0x9B, 0xF3, 0x13, 0xF5, 0x97, 0x41, 0xD0, 0x57, 0x13, 0x1E, 0x6C, 0x4A };
        private static readonly byte[] _rcmpBadBytes = new byte[] { 0x68, 0x56, 0xF9, 0x1D, 0x39, 0x5F, 0xD0, 0x57, 0x13, 0x9B, 0xF3, 0x13, 0xF5, 0x97, 0x41, 0xF5, 0x97, 0x41, 0x1E, 0x6C, 0x4A };

        // Known to cause negative index bug writing undefined bytes:
        private static readonly byte[] _origBadIndex = new byte[] { 0xFC, 0xFC, 0xC2, 0x92, 0x8F, 0x66, 0x33, 0x44, 0xEF, 0x40, 0xE9, 0xAB, 0x44, 0xEF, 0x40, 0x36 };
        private static readonly byte[] _rcmpBadIndex = new byte[] { 0xFC, 0xFC, 0xC2, 0x92, 0x8F, 0x66, 0x33, 0x44, 0xEF, 0x40, 0xE9, 0xAB, 0x00, 0x00, 0x00, 0x36 };

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [TestMethod]
        public void TestBadBytes() => Assert.IsTrue(Recompress(_origBadBytes).SequenceEqual(_rcmpBadBytes));

        [TestMethod]
        public void TestBadIndex() => Assert.IsTrue(Recompress(_origBadIndex).SequenceEqual(_rcmpBadIndex));

        [TestMethod]
        public void TestRandom()
        {
            const int runs = 1000;
            int fails = 0;
            Random random = new Random();
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                for (int i = 0; i < runs; i++)
                {
                    byte[] data = new byte[random.Next(1, 1000)];
                    rng.GetBytes(data);
                    ReadOnlySpan<byte> recompressed = Recompress(data);
                    if (!data.AsSpan().SequenceEqual(recompressed))
                        fails++;
                }
            }
            // If less than 1% fails, the compression is seen as working as in the original implementation.
            Assert.IsTrue(fails / (float)runs < 0.01);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static ReadOnlySpan<byte> Recompress(ReadOnlySpan<byte> data)
        {
            ReadOnlySpan<byte> compressed = PacketCompression.Compress(data);
            return PacketCompression.Decompress(compressed);
        }
    }
}
