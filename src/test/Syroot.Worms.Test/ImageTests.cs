using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.Worms.Test
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="Img"/> class.
    /// </summary>
    [TestClass]
    public class ImageTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in any the test directory.
        /// </summary>
        [TestMethod]
        public void TestImages()
        {
            string[] brokenFiles = new[]
            {
                // Broken compressed image data requiring 3 more bytes than usual.
                "WA\\3.7.2.1\\training1.img", // WA Training, girder map
                "WA\\3.7.2.1\\training2.img", // WA Training, girder map
                "WA\\3.7.2.1\\training3.img", // WA Training, girder map
                "WA\\3.7.2.1\\training4.img", // WA Training, girder map
                "WA\\3.7.2.1\\training5.img", // WA Training, girder map
                "WA\\3.7.2.1\\training6.img", // WA Training, girder map
                "WA\\3.7.2.1\\training7.img", // WA Training, girder map
                "WA\\3.7.2.1\\training8.img", // WA Training, girder map
                "WA\\3.7.2.1\\training9.img", // WA Training, girder map
                "WWP\\mission17.img" // WWP Parachute Problems, Snow map
            };

            Tools.TestFiles<Img>("*.img", brokenFiles);

            // Broken compressed image data requiring 3 more bytes than usual.
            foreach (string fileName in Directory.EnumerateFiles("Files", "*.img", SearchOption.AllDirectories))
            {
                if (brokenFiles.Any(x => fileName.Contains(x, StringComparison.InvariantCultureIgnoreCase)))
                {
                    using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    Assert.ThrowsException<IndexOutOfRangeException>(() => Tools.TestStream<Img>(fileName, stream));
                }
            }
        }
    }
}
