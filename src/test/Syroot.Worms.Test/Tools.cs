using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.IO;

namespace Syroot.Worms.Test
{
    /// <summary>
    /// Represents methods helping in executing file-based tests.
    /// </summary>
    public static class Tools
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads, saves, reloads, and compares the data in the given <paramref name="stream"/>, as long as each
        /// operation is supported.
        /// </summary>
        /// <typeparam name="T">The type of the data to load.</typeparam>
        /// <param name="name">Name logged to output.</param>
        /// <param name="stream"><see cref="Stream"/> storing the raw data to load.</param>
        public static void TestStream<T>(string name, Stream stream)
            where T : ILoadable, new()
        {
            Debug.Write($"\"{name}\" load");
            T instance = new T();
            instance.Load(stream);

            if (instance is ISaveable saveable)
            {
                Debug.Write($" save");
                using MemoryStream newStream = new MemoryStream();
                saveable.Save(newStream);

                Debug.Write($" reload");
                newStream.Position = 0;
                T newInstance = new T();
                newInstance.Load(newStream);

                if (instance is IEquatable<T> equatable)
                {
                    Debug.Write($" compare");
                    Assert.IsTrue(((IEquatable<T>)newInstance).Equals(equatable));
                }
            }

            Debug.WriteLine($" OK");
        }

        /// <summary>
        /// Loads, saves, reloads, and compares the files found with the given <paramref name="wildcard"/>, as long as
        /// each operation is supported. Excludes file names specified in the optional array
        /// <paramref name="excludedFiles"/>.
        /// </summary>
        /// <typeparam name="T">The type of the files to load.</typeparam>
        /// <param name="games">The games to test.</param>
        /// <param name="wildcard">The wildcard to match.</param>
        /// <param name="excludedFiles">Optionally, the files to exclude.</param>
        public static void TestFiles<T>(string wildcard, string[]? excludedFiles = null)
            where T : ILoadable, new()
        {
            string[] fileNames = Directory.GetFiles("Files", wildcard, SearchOption.AllDirectories);
            if (fileNames.Length == 0)
                throw new InvalidOperationException("No files found for test.");

            foreach (string fileName in fileNames)
            {
                if (excludedFiles?.Any(x => fileName.Contains(x, StringComparison.InvariantCultureIgnoreCase)) == true)
                {
                    Debug.WriteLine($"\"{fileName}\" skipped");
                }
                else
                {
                    using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    TestStream<T>(fileName, stream);
                }
            }
        }
    }
}
