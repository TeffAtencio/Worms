using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Armageddon.ProjectX;

namespace Syroot.Worms.Test.Armageddon.ProjectX
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="Library"/> class.
    /// </summary>
    [TestClass]
    public class LibraryTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestLibraries() => Tools.TestFiles<Library>("*.pxl");
    }
}
