using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Armageddon;

namespace Syroot.Worms.Test.Armageddon
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="GeneratedMap"/> class.
    /// </summary>
    [TestClass]
    public class GeneratedMapTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestGeneratedMaps() => Tools.TestFiles<GeneratedMap>("*.lev");
    }
}
